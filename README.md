Build and publish it locally with:

```
sbt publishLocal
```

then you can use the excellent coursier to launch it:

```
coursier launch com.gu:esquery_2.12:0.1.0-SNAPSHOT -- --help
```

or use coursier's bootstrap command to save it permentently somewhere.

Configuration file is `~/.config/esquery/config`.
