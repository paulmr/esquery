VERSION=$(perl -ne 'if(/:= "(.*)"/) { print $1 }' <version.sbt)

echo "Version: $VERSION"
coursier bootstrap com.gu:esquery_2.12:$VERSION -o ~/bin/esquery -f
