package object esquery {
  type ResultsProcessor = (Response) => Response

  type Query = ujson.Obj
  type Response = ujson.Value

}
