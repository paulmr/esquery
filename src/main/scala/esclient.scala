package esquery

import ujson.Js
import scala.util.Try

trait Logger {
  def apply(s: String): Unit
}

object ResultsProcessors {

  private def proc(r: ResultsProcessor) = r

  val defaultResultsProcessor = proc(identity)

  val countResults = proc { res =>
    val hitCount = res("hits")("total").num.toLong
    ujson.Str(s"Total results: $hitCount")
  }

  def aggLookup(aggName: String) = proc(_("aggregations")(aggName))

  val listAggs = proc(_("aggregations").obj.keySet)

}

trait QueryDef {
  def jsonPost(url: String, body: ujson.Value, log: Logger) = {
    val bodyString = ujson.write(body)
    log(s"Url: $url; body: $bodyString")
    requests.post(url, headers = Map("Content-Type" -> "application/json"), data =  bodyString)
  }

  def apply(query: Query, log: Logger, url: (String) => String): Either[String, Response]
}

object SearchQuery extends QueryDef {
  def apply(query: Query, log: Logger, url: (String) => String): Either[String, Response] = {
    val bodyString = ujson.write(query)
    val res = jsonPost(url("_search"), query, log)
    if(!res.is2xx) {
      Left {
        ujson.read(res.text)("error") match {
          case ujson.Str(err) => err
          case ujson.Obj(fields) if fields.isDefinedAt("reason") => fields("reason").str
          case any => s"unknown error: <$any>"
        }
      }
    } else Right(ujson.read(res.text).obj)
  }
}

case class ESClient(baseUrl: String, index: Option[String] = None, verbose: Boolean = true)  {
  type Results = Unit

  val log = new Logger {
    def apply(s: String) = if(verbose) println(s)
  }

  def url(endpoint: String): String = url(List(endpoint))
  def url(endpoint: List[String]): String = {
    val path = (index.toList ++ endpoint).mkString("/")
    s"$baseUrl/$path"
  }

  def apply(
    qdef: QueryDef,
    query: Query,
    proc: ResultsProcessor = ResultsProcessors.defaultResultsProcessor
  ) = try {
    qdef(query, log, url).map(proc.apply)
  } catch {
    case _: java.net.UnknownHostException => Left(s"unknown host: $baseUrl")
  }

  def withIndex(index: String) = copy(index = Some(index))

}
