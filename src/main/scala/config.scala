package esquery

import java.io.File
import scala.io.Source

import toml.Value.Str
import toml._
import Codecs._

case class Config(
  index: String = "data",
  url: String = "https://localhost:9200",
  proc: ResultsProcessor = ResultsProcessors.defaultResultsProcessor,
  queryType: QueryDef = SearchQuery,
  query: ujson.Obj = ujson.Obj(),
  servers: Map[String, String] = Map.empty,
  prettyIndent: Int = 2,
  verbose: Boolean = false
)

object Config {

  import Codec._

  val queryNames: Map[String, QueryDef] = Map("search" -> SearchQuery)

  implicit val queryDefCodec =
    Codec[QueryDef] { (value: Value, defaults: Defaults, index: Codec.Index) =>
      value match {
        case Str(s) => queryNames.get(s).toRight(List("queryType") -> s"Unrecognised query: $s")
        case _ => Left(List("queryType") -> "expected string")
      }
    }

  implicit val queryProcCodec =
    Codec[ResultsProcessor] { (value: Value, defaults: Defaults, index: Codec.Index) =>
      Left(List[Codec.Field]() -> "???")
    }

  def parseConfig(s: String): Either[String, Config] = {
    Toml.parseAs[Config](s).left.map {
      case (addr, msg) => s"${addr.mkString(",")}: $msg"
    }
    //   case Left(_, msg) => Left(msg)
    //   case Right(config) => config.merge(base)
    // for(data <- Toml.parse(s)) yield {
    //   Config(
    //     index = data.values.get("index").getOrElse(base.index)
    //   )
    // }
    // Right(base)
  }

  lazy val cmdLineParser = new scopt.OptionParser[Config]("esquery") {

    help("help").text("this help")

    opt[String]("url")
      .text("URL of the elasticsearch server")
      .action((s, c) => c.copy(url = s))

    opt[String]("server")
      .text("lookup a server from the list of short names")
      .action((s, c) =>  c.copy(url = c.servers(s)))

    opt[Unit]('n', "number")
      .text("display the total number of hits instead of returning the actual results")
      .action((_, c) => c.copy(proc = ResultsProcessors.countResults))

    opt[String]('f', "file")
      .text("read the query as json from a file (or `-` for stdin)")
      .action { (f, c) =>
        val input = if(f == "-") Source.fromInputStream(System.in) else {
          Source.fromFile(f.replaceFirst("^~", System.getenv("HOME")))
        }
        c.copy(query = ujson.read(input.mkString).obj)
      }

    opt[Unit]('v', "verbose")
      .text("more verbose output")
      .action((_, c) => c.copy(verbose = true))

    opt[String]("agg")
      .text("lookup a specific aggregation")
      .action((agg, c) => c.copy(proc = ResultsProcessors.aggLookup(agg)))

    opt[Unit]("aggs")
      .text("list the aggregations")
      .action((_, c) => c.copy(proc = ResultsProcessors.listAggs))

    opt[String]("config")
      .hidden()
      .action { (s, orig) =>
        parseConfig(s) match {
          case Right(config) => config
          case Left(msg) =>
            println(s"failed to parse override config, ignoring: $msg")
            orig
        }
      }

    // OParser.sequence(
    //   programName("scopt"),
    //   head("scopt", "4.x"),
    //   // option -f, --foo
    //   opt[Int]('f', "foo")
    //     .action((x, c) => c.copy(foo = x))
    //     .text("foo is an integer property"),
    //   // more options here...
    // )
  }

  def parseArgs(args: Seq[String], baseConfig: Config) = cmdLineParser.parse(args, baseConfig)
}
