package esquery

import java.io.File
import scala.io.Source


object ESQuery {

  lazy val defaultConfigFile = Option(System.getenv("HOME"))
    .flatMap { home =>
      val fname = s"$home/.config/esquery/config"
      if(new File(fname).exists) Some(fname) else None
    }

  lazy val configFileName =
    Option(System.getenv("ESQUERY_CONFIG"))
      .orElse(defaultConfigFile)
      .getOrElse("./esquery.conf")

  def readConfigFile() = {
    if(new File(configFileName).exists()) {
      Config.parseConfig(Source.fromFile(configFileName).mkString) match {
        case Left(err) =>
          println(s"Error parsing configuration file ($configFileName); $err")
          None
        case Right(cfg) =>
          Some(cfg)
      }
    } else Some(Config())
  }

  def main(args: Array[String]): Unit = {
    for {
      baseConfig <- readConfigFile()
      config <- Config.parseArgs(args, baseConfig)
    } {
      val es = new ESClient(
        baseUrl = config.servers.get(config.url).getOrElse(config.url),
        index = Some(config.index),
        verbose = config.verbose
      ).withIndex(config.index)

      es(config.queryType, config.query, config.proc) match {
        case Left(err) => println(s"Error: $err")
        case Right(ujson.Str(s)) => println(s)
        case Right(any) => println(ujson.write(any, config.prettyIndent))
      }
    }
  }
}
