ThisBuild / scalaVersion     := "2.12.8"
ThisBuild / organization     := "com.gu"
ThisBuild / organizationName := "gu"

lazy val root = (project in file("."))
  .settings(
    name := "esquery",
    libraryDependencies ++= Seq(
      "com.lihaoyi" %% "ujson" % "0.7.1",
      "com.lihaoyi" %% "requests" % "0.1.7",
      "com.github.scopt" %% "scopt" % "3.7.1",
      "tech.sparse" %%  "toml-scala" % "0.2.0"
    )
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
